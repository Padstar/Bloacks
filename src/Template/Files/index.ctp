<!-- Some JS --->
<script type="text/javascript">

	$(function() {
		//document.ready -> in case it's needed sometime...
	});
	
	function togglePopup(){
		
		var lay = document.getElementById('popupLayer');
		var pop = document.getElementById('popup');
		
		if(pop.style.display === "none"){
		lay.style.display = "block";
		pop.style.display = "block";
		}else{
		lay.style.display = "none";
		pop.style.display = "none";
		}
	}
	
	function createFolder() {
		
		var ele = document.getElementById('folderName').value;
			location.href += '?fname=' + ele;
	}
  </script>
  

<!-- Folder Creation Popup -->
<div id="popupLayer" style="display: none">
	<div id="popup" style="display: none">
		<p class="xes" onclick="togglePopup()">X</p></br>
		<?php 
			echo $this->Form->create('folderCreate', ['type' => 'file', 'class' => 'form-control-file', 'url' => ['action' => 'addFolder'] + $path]);
			echo '<br />';
			echo $this->Form->input('Please enter the folders name:', ['type' => 'text', 'name' => 'fname']);
			echo '<br />';
			echo $this->Form->submit('Create new folder!', ['class' => 'btn btn-outline-primary']);
			echo $this->Form->end();
			
		?>
	</div>
</div>

<div style="width:90%; margin:20px auto;"><br />


<!-- check if document is reloaded after folder creation -->
<?php 
		if(isset($_GET['fname']) && $_GET['fname'] != ''){
			//$this->redirect(['controller' => 'files', 'action' => 'addFolder'], $_GET['fname']);
		}
		
$this->assign('title', 'File system');
?>

				<!-- Breadcrumb menu -->

<nav aria-label="breadcrumb">
<?php
$chain = "";

	foreach($path as $folder){ 
	$chain .= $folder . DS;
		$this->Breadcrumbs->add($folder, ['controller' => 'files', 'action' => 'index'] + explode(DS, $chain), ['class' => 'breadcrumb-item']);
	}
	echo $this->Breadcrumbs->render(
    ['class' => 'breadcrumb']
	);
 ?> 
</nav>

				<!-- Folder Create Button -->
<br />
<div id="alignRight">
	<input type="button" value="New Folder" onclick="togglePopup()"></input>
</div>
 
<table class="table table-borderless table-dark">
<thead style="border-bottom: 1px solid gray;">
	<tr><th scope="col" >Item</th><th scope="col">Actions</th></tr>
</thead>
<tbody>
	<?php
	
				//Setting actions for the different items
	
		foreach($files as $filename => $filetype) {
			$targetPath = 	[
								'controller' => 'files',
								'action' => $filetype == "Folder" ? 'index' : 'download'
							] + $path;
			$targetPath[] = $filename;
		?>
		<tr>
				<!-- Displaying the table -->
			<td><?= $this->Html->link($filename, $targetPath, ['class' => '' . ($filetype == "Folder") ? 'btn btn-outline-warning userFolder' : 'btn btn-outline-dark userFile' ]); ?></td>
			<td><p><?php
				
				$targetPath['action'] = 'download';
				echo $this->Html->link('Download', $targetPath, ['class' => 'btn btn-outline-danger']);
			
				?>  <--->  <?php
				$targetPath['action'] = 'delete';
				echo $this->Html->link('Delete', $targetPath, ['class' => 'btn btn-outline-danger']);
			?></p></td>
			
		</tr>
	<?php }
	
	?>
</tbody>
</table>

<br />

<!-- Upload area -->
<div id="uploadWrap" style="width:50%; align:center">
<?php
	echo $this->Form->create('fileManager', ['type' => 'file', 'class' => 'form-control-file', 'url' => ['action' => 'upload'] + $path]);
	
	echo '<br />';
	echo $this->Form->input('item', ['type' => 'file', 'class' => 'form-control wrapped']);
	echo $this->Form->submit('Upload', ['class' => 'btn btn-outline-primary']);
	echo $this->Form->end();
?>
</div>
</div>