<div id="wrapAdd" style="width:30%; margin:200px auto;" >
<br />
<?php
$this->assign('title', 'Registration');
	echo $this->Form->create('User', ['class' => 'form-signin']);
	echo $this->Form->control('username', ['class' => 'form-control']);
	echo '<br />';
	echo $this->Form->control('password', ['class' => 'form-control']);
	echo '<br />';
	
	if($isadmin){
		echo $this->Form->control('is Admin? ', ['type' => 'checkbox']);
	}
	
	echo $this->Form->submit('Register Account!', ['class' => 'btn btn-outline-primary']);
	echo $this->Form->end();
?>
</div>
