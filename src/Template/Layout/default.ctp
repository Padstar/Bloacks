<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>
        <?= $this->fetch('title') ?>
    </title>
	<?= $this->Html->meta('icon') ?>
	<?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
	
	<!-- jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	
	<!-- main.css for adjusting things...if needed... -->
	<?= $this->Html->css('main') ?>
	<?= $this->Html->css('https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css', ['integrity' => 'sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO', 'crossorigin' => 'anonymous']) ?>
	<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', ['integrity' => 'sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49', 'crossorigin' => 'anonymous']);?>
	<?= $this->Html->script('https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', ['integrity' => 'sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy', 'crossorigin' => 'anonymous']);?>
	
	
    
	
</head>
<body>
    <nav class="navbar navBarDiv navbar-expand-lg navbar-dark bg-dark" data-topbar role="navigation">
                <h1 class="navbar-brand "> <?= $this->Html->link($this->fetch('title'), ['controller' => '' . ($loggedin) ? 'files' : 'users', 'action' => '' . ($loggedin) ? 'index' : 'login'], ['class' => 'nav-link headLine']);?> </h1>
				
        <div class="collapse navbar-collapse justify-content-md-center">
            <ul class="nav nav-pills">
                <?php 
				if($loggedin){ ?>
				<li class="nav-item">
				<?php
					echo $this->Html->link('Logout', ['controller' => 'users', 'action' => 'logout'], ['class' => 'nav-link active']); ?></li><?php
					if($isadmin){?>
					<li class="nav-item">
					<?php
					echo $this->Html->link('Register!', ['controller' => 'users', 'action' => 'add'], ['class' => 'nav-link active', 'style' => 'margin-right:10px']); ?></li><?php
					}
				}else{ ?>
				<li class="nav-item"><?php
					echo $this->Html->link('Login', ['controller' => 'users', 'action' => 'login'], ['class' => 'nav-link']);?></li>
					<li class="nav-item">
					<?php
					echo $this->Html->link('Register!', ['controller' => 'users', 'action' => 'add'], ['class' => 'nav-link active', 'style' => 'margin-right:10px']); ?></li><?php 
					
				}
  ?>
            </ul>
        </div>
    </nav>
	<div id="resultMessageBox">
		<?= $this->Flash->render() ?>
	</div>
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    <footer>
    </footer>
	</body>
</html>
