<?php
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Auth;


class AppController extends Controller
{
	public $session = null;
	
	
	public function beforeFilter(Event $event)
	{
		$this->set('isadmin', !is_null($this->Auth->user()) && $this->Auth->user()['isAdmin']);
		$this->set('loggedin', !is_null($this->Auth->user()));
	}
	
    public function initialize()
    {
        parent::initialize();
		$this->session = $this->getRequest()->getSession();
		
        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
		$this->loadComponent('Auth', [
		'authenticate' => [
			'Form' => [
				'fields' => [
					'username' => 'username',
					'password' => 'password'
				]
			]
		],
		'loginAction' => ['controller' => 'users', 'action' => 'login']
		]);
    }
}
