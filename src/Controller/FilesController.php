<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Utility\Hash;
use Cake\Utility\Security;
use Cake\View\Helper\BreadcrumbsHelper;
use Cake\Filesystem\ZipArchive;
use Cake\Collection\Iterator;
use Cake\Collection\Iterator\TreeIterator;

class FilesController extends AppController
{
	// Generates the absolute path of an Item if param is true
	// Otherwise return relative path
	private function generatePath($absolute = true){
		$relPath = implode(DS, $this->request->params['pass']);
		return $absolute ? $this->session->read('Cloud.userDir') . $relPath : $relPath;
	}
	
	//Returns all files from given directory, sets the types and sorts the array so Folders will be first for the view
	private function getFilesFromDir($absolutePath){
		$files = [];
		
		foreach(scandir($absolutePath) as $file) {
			if($file == "." || $file == "..") continue;
			$files[$file] = is_dir($absolutePath  . DS . $file)  ? "Folder" : "File";
		}
		arsort($files);
		return $files;
	}
	
	
	
    public function index()
	{
		$absolutePath = $this->generatePath();
		$files = $this->getFilesFromDir($absolutePath);
		
		$this->set('path', $this->request->params['pass']);
		$this->set('user', $this->Auth->user());
		$this->set('files', $files);
	}
	
	
	//Inserts a new Folder for the active directory
	public function addFolder($name = '') {
		
		if($this->request['data']['fname'] != '')
			$name = $this->request['data']['fname'];
		else
			$name = 'newFolder_' . Security::randomString(8);
		
		mkdir($this->generatePath(true) . DS . $name, 0755);
		//$dir = new Folder($this->generatePath(true) . DS . $name, 2);
		$this->redirect(['action' => 'index'] + $this->request->params['pass']);
		
	}
	
	
	
	// File upload (folders are not working that easily)
	public function upload(){
		// only allow post
		if(!$this->request->is('post')){
			$this->Flash->error('Error! Couldn\'t find file!');
			return;
		}
		
		//Restriction for upload in ROOT!!
		if(!$this->request->params['pass'] == []){
			
			$resultMsg = move_uploaded_file($_FILES['item']['tmp_name'], $this->generatePath(implode(DS, $this->request->params['pass'])) . DS . $_FILES['item']['name']);
		}else{
			$resultMsg = false;
		}
		
		if($resultMsg)
			$this->Flash->success('Upload successful!');
		else
			$this->Flash->error('No Root upload available!');

		return $this->redirect(['action' => 'index'] + $this->request->params['pass']);
	}
	
	// Download of files and folders
	public function download(){
		
		$path = $this->generatePath();		
		if(is_dir($path)){
			$targetZip = TMP . 'bla.zip';
			
			if(is_file($targetZip))
				unlink($targetZip);
			
			$zip = new \ZipArchive();
			$zip->open($targetZip, \ZipArchive::CREATE);
			
			$files = Folder::tree($path, false, false);
			foreach($files as $file){
					$zip->addFile($file, substr($file, strlen($this->session->read('Cloud.userDir'))));
			}
			$zip->close();
			
			return $this->response->withFile(
					$targetZip,
					['download' => true, 'name'=> basename($path) . ".zip"]
				);
		}else{
			return $this->response->withFile(
					$path,
					['download' => true, 'name'=> basename($path)]
				);
		}
	}
	
	// Deletes files and (empty-)folders
	public function delete($file){
		
		$path = $this->generatePath();

		if(!is_dir($path)){
			unlink($path);
			$this->Flash->success("File deleted!");
		}else{
			rmdir($path);
			$this->Flash->success("Folder deleted!");
		}
		$var = $this->request->params['pass'];
		array_pop($var);
		$this->redirect(['action' => 'index'] + $var);
		
	}
}