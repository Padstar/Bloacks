<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;


class UsersController extends AppController
{
	
	public function beforeFilter(Event $event)
	{
		$this->Auth->authorize = 'controller';
		
		if(is_null($this->Auth->user())) {
			$this->Auth->deny('all');
			$this->Auth->allow(['login', 'add']);
		}
		
		return parent::beforeFilter($event);		
	}
	
	public function afterIdentify($event, $user)
	{
		if($user['isAdmin'] == 0){
			$userDir = Configure::read('Cloud.storagefilename') . $user['username'] . DS;
			$this->session->write('Cloud.userDir', $userDir);
		}else{
			$userDir = Configure::read('Cloud.storagefilename');
			$this->session->write('Cloud.userDir', $userDir);
		}
	}
	
	public function implementedEvents()
	{
		return [
			'Auth.afterIdentify' => 'afterIdentify'
		] + parent::implementedEvents();
	}
	
	
	
	public function login(){
		if ($this->request->is('post')) {
			$user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
				
				//creating base folder in case Admin deleted it...xD
					if(!is_dir(Configure::read('Cloud.storagefilename') . $user['username'])){
						mkdir(Configure::read('Cloud.storagefilename') . $user['username']);
					}
				
				$this->Flash->success('Login successful! Hey ' . $this->Auth->user()['username'] . '!');
                return $this->redirect(['controller'=>'files','action'=>'index']);
            }else{
            $this->Flash->error(__('Login failed, please try again!'));
			}
		}
	}
	
	public function logout()
    {
		$this->getRequest()->getSession()->destroy();
        return $this->redirect($this->Auth->logout());
    }

	
	public function add(){
		$user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
			
			if($user['is_Admin?_']	== 1)
			$user->isAdmin = 1;
		else
			$user->isAdmin = 0;
			
			$user->created = date("Y-m-d");
				if ($this->Users->save($user)) {
					$this->Flash->success(__('The user has been saved.'));
					
					//creating base folder for each newly created user
					if(!is_dir(Configure::read('Cloud.storagefilename') . $user->username)){
						mkdir(Configure::read('Cloud.storagefilename') . $user->username);
					}
					return $this->redirect(['controller' => 'users', 'action' => 'add']);
			}else{
				$this->Flash->error(__('Unable to add the user.'));
			}
		}
		$this->set('user', $user);
	}
}
